Source: lomiri-app-launch
Section: gnome
Priority: optional
Maintainer: UBports developers <developers@ubports.com>
Build-Depends: abi-compliance-checker,
               click-dev (>= 0.2.2),
               cmake,
               cmake-extras (>= 0.10),
               dbus-test-runner,
               debhelper-compat (= 12),
               dh-migrations | hello,
               gir1.2-gobject-2.0-dev,
               googletest,
               libclick-0.4-dev,
               libcurl4-gnutls-dev,
               libdbus-1-dev,
               libdbustest1-dev (>= 14.04.0),
               libertined (>= 1.6) <!pkg.lomiri-app-launch.bootstrap>,
               libgirepository1.0-dev,
               libglib2.0-dev,
               libgtest-dev,
               libjson-glib-dev (>= 1.1.2),
               liblibertine-dev <!pkg.lomiri-app-launch.bootstrap>,
               liblttng-ust-dev,
               libmir1client-dev (>= 0.5) | hello,
               libproperties-cpp-dev,
               liblomiri-api-dev (>= 0.1.0),
               libzeitgeist-2.0-dev,
               gobject-introspection,
               python3-dbusmock,
               systemd,
Standards-Version: 4.5.0
Rules-Requires-Root: no
Homepage: http://gitlab.com/ubports/core/lomiri-app-launch

Package: lomiri-app-launch
Architecture: any
Depends: ${shlibs:Depends},
         ${misc:Depends},
         click-apparmor,
         dbus-user-session,
         zeitgeist-core,
Multi-Arch: foreign
Description: User space daemon for launching applications
 Application launching system and associated utilities that is used to
 launch applications in a standard and confined way.
 .
 This package provides the Lomiri App Launch user space daemon.

Package: lomiri-app-launch-tools
Architecture: any
Depends: ${shlibs:Depends},
         ${misc:Depends},
         lomiri-app-launch (= ${binary:Version})
Replaces: upstart-app-launch-tools,
          ubuntu-app-launch-tools,
Provides: upstart-app-launch-tools,
          ubuntu-app-launch-tools,
Description: Tools for working wtih launched applications
 Application launching system and associated utilities that is used to
 launch applications in a standard and confined way.
 .
 This package provides tools for working with Lomiri App Launch.

Package: liblomiri-app-launch0
Section: libs
Architecture: any
Depends: ${misc:Depends},
         ${shlibs:Depends},
         lomiri-app-launch (= ${binary:Version}),
#         lomiri-pasted,
Recommends:
         libertine-xmir-tools [amd64 armhf arm64 i386],
Pre-Depends: ${misc:Pre-Depends},
Multi-Arch: same
Description: library for sending requests to Lomiri App Launch
 Application launching system and associated utilities that is used to
 launch applications in a standard and confined way.
 .
 This package contains shared libraries to be used by applications.

Package: liblomiri-app-launch-dev
Section: libdevel
Architecture: any
Depends: ${misc:Depends},
         ${shlibs:Depends},
         gir1.2-lomiriapplaunch-0 (= ${binary:Version}),
         libglib2.0-dev,
         ${libmir1client-dev},
         libproperties-cpp-dev,
         liblomiri-app-launch0 (= ${binary:Version}),
Pre-Depends: ${misc:Pre-Depends},
Multi-Arch: same
Description: library for sending requests to the Lomiri App Launch
 Application launching system and associated utilities that is used to
 launch applications in a standard and confined way.
 .
 This package contains files that are needed to build applications.

Package: gir1.2-lomiriapplaunch-0
Section: introspection
Architecture: any
Depends: ${shlibs:Depends},
         ${misc:Depends},
         liblomiri-app-launch0 (= ${binary:Version}),
         ${gir:Depends},
Pre-Depends: ${misc:Pre-Depends}
Recommends: lomiri-app-launch (= ${binary:Version})
Multi-Arch: same
Description: typelib file for liblomiri-app-launch4
 Application launching system and associated utilities that is used to
 launch applications in a standard and confined way.
 .
 Interface for starting apps and getting info on them.
 .
 This package can be used by other packages using the GIRepository format to
 generate dynamic bindings for liblomiri-app-launch0.
